---
title: "Temperature_preference_analysis"
author: "Manuel Grub"
date: "8/11/2021"
output:
  html_document:
    toc: yes
    theme: united
    toc_float: yes
    float: yes
    number_sections: yes
    toc_depth: 2
    highlight: tango
  pdf_document:
    toc: yes
    toc_depth: '2'
---

This script was used to analyze the data for my temperature preference experiments

# dependencies
```{r}
library(nlme)
library(emmeans)
```



# Preparing the data

```{r}
Df_mixed_mod_lf=read.csv("C:\\Users\\grubm\\OneDrive\\Desktop\\manu\\uni\\MSc1-BEC\\Master Project\\Assays_Temp_Behavior\\Csv_python_analysis\\Df_combined_lf.csv", header=T, sep=",")

Df_mixed_mod_lf$Species=as.factor(Df_mixed_mod_lf$Species)
Df_mixed_mod_lf$Strains=as.factor(Df_mixed_mod_lf$Strains)
Df_mixed_mod_lf$Runs=as.factor(Df_mixed_mod_lf$Runs)


Df_combined_Af_lf=read.csv("C:\\Users\\grubm\\OneDrive\\Desktop\\manu\\uni\\MSc1-BEC\\Master Project\\Assays_Temp_Behavior\\Df_african_lf.csv", header = T, sep=",")

Df_combined_Af_lf$African_runs=as.factor(Df_combined_Af_lf$African_runs)
Df_combined_Af_lf$Species=as.factor(Df_combined_Af_lf$Species)

```


# Japanese species

## Species effect
### Specifiy the model

```{r}

obj.lme=lme(Temperature ~ Species, data = Df_mixed_mod_lf, random = ~ 1 | Runs)

```



### Wald F-test
```{r}
anova(obj.lme)

```

### Likelihood ratio test 

```{r}
obj.lme0.ml=lme(Temperature~1, data= Df_mixed_mod_lf, random = ~ 1|Runs, method="ML") # reduced model
obj.lme.ml=lme(Temperature~1 + Species, data= Df_mixed_mod_lf, random = ~ 1|Runs, method="ML") # full model
anova(obj.lme0.ml, obj.lme.ml)
```



### summarize the effect of each species

```{r}
summary(obj.lme) 

```


### Post hoc comparisons

```{r}
emmeans(obj.lme, pairwise ~ Species)

```
## Strains effect
### Specifiy the model

```{r}

obj.lme_str=lme(Temperature ~ Strains, data = Df_mixed_mod_lf, random = ~ 1 | Runs)

```


### Wald F-test
```{r}
anova(obj.lme_str)

```

### Likelihood ratio test 

```{r}
obj.lme0.ml_str=lme(Temperature~1, data= Df_mixed_mod_lf, random = ~ 1|Runs, method="ML") # reduced model
obj.lme.ml_str=lme(Temperature~1 + Strains, data= Df_mixed_mod_lf, random = ~ 1|Runs, method="ML") # full model
anova(obj.lme0.ml_str, obj.lme.ml_str)
```



### summarize the effect of each strain

```{r}
summary(obj.lme_str) 

```


### Post hoc comparisons

```{r}
emmeans(obj.lme_str, pairwise ~ Strains)

```

# African species

## Species effect
### Specifiy the model


```{r}
obj.lme_Af=lme(Temperatures ~ Species, data = Df_combined_Af_lf, random = ~ 1 | African_runs)
```

### Wald F-test
```{r}
anova(obj.lme_Af)
```


### Likelihood ratio test

```{r}
obj.lme0.ml_af=lme(Temperatures~1, data= Df_combined_Af_lf, random = ~ 1|African_runs, method="ML") # reduced model
obj.lme.ml_af=lme(Temperatures~1 + Species, data= Df_combined_Af_lf, random = ~ 1|African_runs, method="ML") # full model
anova(obj.lme0.ml_af, obj.lme.ml_af)
```


### summarize the effect of each species

```{r}
summary(obj.lme_Af) 


```


### Post hoc comparisons

```{r}
emmeans(obj.lme_Af, pairwise ~ Species)

```