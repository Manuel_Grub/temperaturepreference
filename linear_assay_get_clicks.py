#!/usr/bin/env python3

# This code is based on the code provided by Tanuj Kafle (high GitLab page here: https://gitlab.com/tanekafle).
# The script was modified as follows:
# - input of temperature at the middle and both ends of the arena needed for the temperature matrix (lines 86-102)


# Then we want to open either ALL, SOME, or NECESSARY
    # ALL opens all one by one, some opens every 2 minutes, and necessary every 5 minutes.
    # When pic opens we click on the larvae, and the coordinates save to some list.
        # Once clicking is done, press N for next and the next picture opens. After last photo it is done and we can see the usual stats.


import cv2
import cv2 as cv
import numpy as np
import os
import sys
sys.path.insert(0, 'C:/Users/grubm/OneDrive/Desktop/manu/uni/MSc1-BEC/Master Project/script/') #path to the scripts Tane sent me 
#import get_FLIR_matrix as FLIR # this is not just functions, so it calls script too - could put it in main function. # 4th November, I put it into a main function.
#import smoothspikes as smsp
import datetime
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import get_FLIR_matrix_analysisFunctions as FAF # this is not just functions, so it calls script too - could put it in main function. # 4th November, I put it into a main function.
import thermal_preference_analysis_functions as TPA
import argparse
import math

# # put in cropped images
# imgpath='/Users/tkafle/GitLab/phd/y1/01_arena/analysis_pipeline/Output/linear/Mar21/18/linear/cropped/c.DmelCS-03182021121615-0.tiff'
# picdir="/Users/tkafle/GitLab/phd/y1/01_arena/analysis_pipeline/Output/linear/Mar21/18/linear/cropped"
# tempMat, dateLst, oi = FLIR.load_pickle("/Users/tkafle/GitLab/phd/y1/01_arena/analysis_pipeline/Output/linear/Mar21/18/run_210318_1215_SqTw-DmelCS.crpd.rpy")
# trDime = [457, 1989]
# sp="DmelCS"
# rn="run1"


parser = argparse.ArgumentParser(description='This script')
parser.add_argument('-tmin', '--Temp_minimum', type=float, help='Insert minimum temperature on the cold side of the arena')
parser.add_argument('-tmid', '--Temp_in_middle', type=float, help='Insert the temperature on the middle of the arena')
parser.add_argument('-tmax', '--Temp_maximum', type=float, help='Insert the maximum temperature on the hot side of the arena')
parser.add_argument('-rp', '--Row_pixels', type=int, help='Insert the vertical number of pixels of the images')
parser.add_argument('-cp', '--Column_pixels', type=int, help='Insert the horizontal number of pixels of the images')
parser.add_argument('-d', '--Directory', type=str, help='Insert the path to the cropped images')
parser.add_argument('-rn', '--Run', type=str, help='Insert the number of the run')
parser.add_argument('-sp', '--Species', type=str, help='Insert the species of the run')
parser.add_argument('-o', '--output', type=str, help='Insert the output path')
parser.add_argument('-l', '--Duration_time', type=int, help='Insert the duration time of the run', default=20)
args = parser.parse_args()





def rotate_tempData(temperature_matrix):
    '''
    Rotate tempdata 90˚ anti-clockwise.
    '''
    rotated1 = np.array(list(zip(*reversed(temperature_matrix))))
    return rotated1

def flip_tempData(temperature_matrix):
    flipped1 = temperature_matrix[::-1]
    return flipped1

def show_tempData(temperature_matrix):
    newmap = FAF.discrete_cmap(11, "gist_gray")
    TPA.heatmap4bg(temperature_matrix, cmap=newmap) 
    plt.show()

#old method of doing the matrix

#matrix=[] #define empty matrix
 
#for i in range(args.Row_pixels): #number of 
#    row=[] 
#    for j in range(args.Column_pixels): #insert the total number of rows (number of pixels in vertical)
#        row.append(np.linspace(args.Temp_maximum, args.Temp_minimum, num=args.Column_pixels)) 
#    matrix.append(row) #add fully defined column into the row


#tried this new method

Count=args.Column_pixels/2

Half_col=math.floor(Count)


lst=[0]*args.Column_pixels
lst[0:Half_col]=np.linspace(args.Temp_maximum, args.Temp_in_middle, num=Half_col)
lst[Half_col: args.Column_pixels]=np.linspace(args.Temp_in_middle, args.Temp_minimum, num=Half_col)
vec=np.array(lst)


w, h = args.Column_pixels, args.Row_pixels;
matrix = [[0 for x in range(w)] for y in range(h)]

for i in range(args.Column_pixels):
    for j in range (args.Row_pixels):
        matrix[j][i]=vec

        

#if len(tempMat[0]) < len(tempMat[0][0]): 
    #raise Exception("The thermal camera data should be portrait. This one is landscape.\nTry functions: rotate_tempData, flip_tempData and show_tempData to make sure it's in the right oriention")
#else:
 #   print("Temperature matrix is in right orientation")
#show_tempData(tempMat[5])
#tempMat_smoothed = smsp.applySmoothSpikes(tempMat)

arena_tempdata  = matrix #could make a matrix with the temperatures of the arena (has to have the same size in pixels)
# show_tempData(arena_tempdata[5])



# load names of all images in directory, get first image and from the time, get 5m, 10m, 15m, 20m, 25m, 30m.

# directory = os.path.dirname(imgpath)
directory = args.Directory #directory to the images 
# outdir = os.path.join(directory , "cropped")

frmz = []
for frame in os.scandir(directory): # https://www.newbedev.com/python/howto/how-to-iterate-over-files-in-a-given-directory/
    if frame.path.endswith(".jpg") and frame.is_file(): #changed .tiff to .jpg 
        frmz.append(frame.name)
        # read in image
        # cutImage = cv2.imread(os.path.abspath(frame)    )
        # warped = four_point_transform(cutImage, np.array(refPt), sq = args["square"])
        # outfilename=os.path.join(outdir, 'c.' + os.path.basename(frame) )
        # print(os.path.basename(frame) + "\r", end = "")
        # cv2.imwrite(outfilename, warped)

frmz.sort()

#frmzdate = [datetime.datetime.strptime(f.split(".")[-2].split("-")[-2],"%m%d%Y%H%M%S") for f in frmz ] # getting time of picture being taken.

#d2fi = [time - frmzdate[0] for time in frmzdate ] # difference to first image in seconds


#mww = [0,5,10,15,20,25,30] # minutes we want
#mwws = [n * 60 for n in mww] # minutes we want in seconds

#desf = [min(d2fi, key=lambda x:abs(x - datetime.timedelta(seconds=t))) for t in mwws] # desired frames based on minutes we want.
#idesf = [d2fi.index(frmval) for frmval in desf] # index of desired frames
#d2fi.index(datetime.timedelta(seconds=310))

#frmz = np.array(frmz)
#dfrmz = frmz[idesf] # desired frames


lvlareas = {}
lvlcentr = {}
i=0


refPt = []
# cropping = False
def clickpoints(event, x, y, flags, param):
	# grab references to the global variables
    global refPt
    global i
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt.append((x, y))
        cv2.circle(image,refPt[i], 2, (0,0,255), -1)
        i += 1
        cv2.imshow("image", image)
		# draw a rectangle around the region of interest
		# cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
    # if len(refPt) == 4:
    #     cv2.circle(image,refPt[0], 5, (0,0,255), -1)
    #     cv2.circle(image,refPt[1], 5, (0,0,255), -1)
    #     cv2.circle(image,refPt[2], 5, (0,0,255), -1)
    #     cv2.circle(image,refPt[3], 5, (0,0,255), -1)
    #     cv2.imshow("image", image)


imgrefs = {}
for imgz in frmz:
    fullimg = directory + "/" + imgz 

    # if imgpath is not None:
    image = cv2.imread(fullimg)

    clone = image.copy()
    cv2.namedWindow("image",cv2.WINDOW_NORMAL)
    cv2.resizeWindow("image", args.Column_pixels+100, args.Row_pixels+50)  #if we want the image to be of a particular size when clicking 
    cv2.setMouseCallback("image", clickpoints)
    # keep looping until the 'n' key is pressed
        # display the image and wait for a keypress
        # if the 'r' key is pressed, reset the cropping region
            # if the 'c' key is pressed, break from the loop

    while True:
        cv2.imshow("image", image)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("r"):
            image = clone.copy()
            refPt = []
            i = 0
        elif key == ord("n"):
            imgrefs[imgz] = refPt
            refPt = []
            i = 0
            break

    cv2.destroyAllWindows()





def get_temps(coordsDict, arena_tempdata):
    '''
    This function will extract the temperature a particular larva was at by inputting coordinate data.
    It will take in the tracking dataframe from FIMtracker and automatically take the coordinates from the centre of mass.
    If you choose either head or tail (never both) to be True, it will take the coordinates of the one you set to be true.
    '''
    temperatures = {}
    for i,n in enumerate(coordsDict):
        tmp4frm = []
        for lc in coordsDict[n]:
            tempMat = arena_tempdata
            t4l = tempMat[0][int(lc[1])][int(lc[0])] # make function that detects closest tempMat using dateLst
            tmp4frm.append(t4l)
        temperatures[n] = tmp4frm
    return temperatures




#dateLst = [datetime.datetime.strptime(tcdate,"%y%m%d_%H%M%S") for tcdate in dateLst]
#tempDateindex = [TPA.nearestTime(dateLst, framedate) for framedate in frmzdate]
#tempDateindex = np.array(tempDateindex)
#tempDateindex = tempDateindex[idesf]


lvltemps = get_temps(imgrefs, arena_tempdata)

lt2keys = sorted(lvltemps.keys()) 
xl = []
yl = []
el = []
for val in lt2keys:
    xl.append(val)
    yl.append(np.mean(lvltemps[val]))
    el.append(np.std(lvltemps[val]))

# set x limit to max temp and min temp
plt.errorbar(xl, yl, el, linestyle='None', marker='^')
plt.show()

tpdf = pd.DataFrame.from_dict(lvltemps, orient="index").transpose()
cols = tpdf.columns.tolist()
cols.sort()
tpdf = tpdf[cols]
# cols = [f.split(".")[-2].split("-")[-1] for f in cols]
#cols = {cols[0]:'0', cols[1]:'5', cols[2]:'10', cols[3]:'15', cols[4]:'20', cols[5]:'25', cols[6]:'30'}
#tpdf = tpdf.rename(cols, axis='columns')nnnnn
#tpdf["run"] = [args.Run] * len(tpdf) #add rn to the variables  #removed this
#tpdf["species"] = [args.Species] * len(tpdf) #add to the arguments  #removed this
tpdf.to_csv(f"{args.output}{args.Species}{args.Run}_table.click.csv") 
np.save(f"{args.output}{args.Species}{args.Run}_coords.click.npy", imgrefs) 
# read_dictionary = np.load(f"/Users/tkafle/GitLab/phd/y1/01_arena/analysis_pipeline/Output/linear/{sp}{rn}_coords.click.npy",allow_pickle='TRUE').item()
g = sns.catplot(data=tpdf.select_dtypes(include='number'), kind = "strip")
g.set_xticklabels(rotation=90)
plt.show()




